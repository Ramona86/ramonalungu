package week12;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import week06.MetricConvertor;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.sql.*;

class MainTest {
     private Connection connection;

    @BeforeEach

    void setUp(){
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BookingApp", "postgres", "parola12345");
        } catch (SQLException e) {
            System.err.println("Can NOT connect to DB" + e.getMessage());
        }

        if (connection == null) {
            return;
        }


    }

//
//    @Test
//    void insertIntoTable_fail(){
//        PreparedStatement preparedStatement = null;
//        try {
//            preparedStatement = connection.prepareStatement("insert into accommodation values (?,?,?, ?, ?)");
//            preparedStatement.setInt(1, 1);
//            preparedStatement.setString(2, "Superior King Room");
//            preparedStatement.setString(3, "1 large double bed");
//            preparedStatement.setInt(4, 2);
//            preparedStatement.setString(5, "This double room has a flat-screen TV, bathrobe and soundproofing.");
//            preparedStatement.executeUpdate();
//        } catch (
//                SQLException e) {
//            System.err.println("Can not insert accommodation " + e.getMessage());
//        }
//    }

    @Test
    public void testArray() throws SQLException {
        PreparedStatement prep = connection.prepareStatement("select * from table room_fair order by value");
        prep.setObject(1, new Object[] { new BigDecimal("1"), "2" });
        ResultSet rs = prep.executeQuery();
        rs.next();
        assertEquals("1", rs.getString(1));
        rs.next();
        assertEquals("2", rs.getString(1));
        assertFalse(rs.next());
    }

    }

