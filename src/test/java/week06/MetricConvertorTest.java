package week06;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class MetricConvertorTest {

    private MetricConvertor metricConvertor;

    @BeforeEach
    void setUp(){
        metricConvertor = new MetricConvertor();
    }


    @ParameterizedTest
    @ValueSource(ints={0})
    void convertIntoMmFromDm_convert_fails(int numberToConvert) {
        metricConvertor.setUnitToConvert("dm");
        metricConvertor.convertIntoMmFromDm();
        metricConvertor.setNumberToConvert(numberToConvert);
        System.out.println(numberToConvert);
        assertFalse(Boolean.parseBoolean("operation can not be computed if number is 0"));
    }

    @Test
    void convertIntoMmFromKm_convert_success(){
        metricConvertor.setUnitToConvert("km");
        metricConvertor.setNumberToConvert(100);
        metricConvertor.convertIntoMmFromKm();
        assertEquals(100000000, metricConvertor.getConvertedNumber());
    }


    @Test
    void convertIntoMmFromM_convert_success(){
        metricConvertor.setUnitToConvert("m");
        metricConvertor.setNumberToConvert(18);
        metricConvertor.convertIntoMmFromM();
        assertEquals(18000, metricConvertor.getConvertedNumber());
    }
}