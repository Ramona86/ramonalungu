package week05;

import week05.hobbies.Address;
import week05.hobbies.Country;
import week05.hobbies.Hobbies;
import week05.personclasses.Hired;
import week05.personclasses.Person;
import week05.personclasses.Student;
import week05.personclasses.Unemployed;
import week05.hobbies.comparators.PersonAgeComparator;
import week05.hobbies.comparators.PersonNameComparator;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        PersonNameComparator personNameComparator = new PersonNameComparator();
        PersonAgeComparator personAgeComparator = new PersonAgeComparator();

        Person person1 = new Student("Mihai", "20");
        Person person2 = new Hired("Ana", "25");
        Person person3 = new Unemployed("Dan", "21");
        Person person4 = new Student("Mihai", "20");
        Person person5 = new Student ("Adi", "20");
        Person person6 = new Student("Mihai", "31");

        TreeSet<Person> persons = new TreeSet<Person>(personNameComparator);
        TreeSet<Person> persons1 = new TreeSet<Person>(personAgeComparator);

        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);
        persons.add(person6);

        persons1.add(person1);
        persons1.add(person2);
        persons1.add(person3);
        persons1.add(person4);
        persons1.add(person5);
        persons1.add(person6);

        System.out.println("List of persons is composed by " + persons);

        for(Person person: persons){
            System.out.println("Person name is: " + person.getName() + " Person age is " + person.getAge());
        }

        List <Hobbies> hobbiesList = new ArrayList<>();

        Hobbies hobby1 = new Hobbies();
        hobby1.setCycling("cycling");
        hobby1.setFrequency(Integer.parseInt("2"));
        Hobbies hobby2 = new Hobbies();
        hobby2.setSinging("singing");
        hobby2.setFrequency(Integer.parseInt("4"));
        Hobbies hobby3 = new Hobbies();
        hobby3.setDancing("dancing");
        hobby3.setFrequency(Integer.parseInt("6"));

        hobbiesList.add(hobby1);
        hobbiesList.add(hobby2);
        hobbiesList.add(hobby3);

        System.out.println("List of hobbies is composed by " + hobbiesList);

        for(Hobbies hobby: hobbiesList){
            System.out.println("Hobbies are " +hobby);
        }

//        Hobby 1

        Address address1 = new Address("Calea Girocului", "87", "Timisoara");
        Address address2 = new Address("Dover Street", "8", "Zurich");
        Address address3 = new Address("Aldred Road", "2", "Nice");

        hobby1.addressList.add(address1);
        hobby1.addressList.add(address2);
        hobby1.addressList.add(address3);


        Country country1 = new Country("Romania", "Maros Bike Cup");
        Country country2 = new Country("Switzerland", "Mountain Bike");
        Country country3 = new Country("France", "Alien Cycling");

        hobby1.countryList.add(country1);
        hobby1.countryList.add(country2);
        hobby1.countryList.add(country3);

//  Hobby 2

        Address address4= new Address("Mangaliei", "7", "Bucuresti");
        Address address5 = new Address("Dover Stret", "8", "Londra");
        Address address6 = new Address("Aldred Road", "2", "Nice");

        hobby2.addressList.add(address4);
        hobby2.addressList.add(address5);
        hobby2.addressList.add(address6);

        Country country4 = new Country("Romania", "Vocea Romaniei");
        Country country5 = new Country("England", "X Fctor");
        Country country6 = new Country("France", "Next star");

        hobby2.countryList.add(country4);
        hobby2.countryList.add(country5);
        hobby2.countryList.add(country6);


        //  Hobby 3

        Address address7= new Address("Colmarkt", "7", "Viena");
        Address address8 = new Address("Dover Stret", "8", "Munchen");
        Address address9 = new Address("Aldred Road", "2", "Dunlin");


        hobby3.addressList.add(address7);
        hobby3.addressList.add(address8);
        hobby3.addressList.add(address9);


        Country country7 = new Country("Austria", "Dance all stars");
        Country country8 = new Country("Germany", "Dance your style");
        Country country9 = new Country("Ireland", "Best dancer");

        hobby3.countryList.add(country7);
        hobby3.countryList.add(country8);
        hobby3.countryList.add(country9);

        System.out.println("The address list for Hobby1 is composed by " +hobby1.addressList);
        System.out.println("The address list for Hobby2 is composed by " +hobby2.addressList);
        System.out.println("The address list for Hobby3 is composed by " +hobby3.addressList);

        System.out.println("The country list for Hobby1 is composed by " +hobby1.countryList);
        System.out.println("The country list for Hobby2 is composed by " +hobby2.countryList);
        System.out.println("The country list for Hobby3 is composed by " +hobby3.countryList);

        System.out.println("Hobby 1 details: " + hobby1);
        System.out.println("Hobby 2 details: " + hobby2);
        System.out.println("Hobby 3 details: " + hobby3);


        Map<Person, List<Hobbies>> hobbiesHashMap = new HashMap<>();
        hobbiesHashMap.put(person1, hobbiesList);

        System.out.println(hobbiesHashMap);

        for (Person i : hobbiesHashMap.keySet()) {
            System.out.println(i);
        }
        System.out.println("hello");
    }


}
