package week05.personclasses;

public class Student extends Person {

    public Student(String name, String age) {
        super(name, Integer.parseInt(age));
    }
}
