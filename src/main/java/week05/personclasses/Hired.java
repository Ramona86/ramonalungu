package week05.personclasses;

public class Hired extends Person {

    public Hired(String name, String age) {
        super(name, Integer.parseInt(age));
    }
}
