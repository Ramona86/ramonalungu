package week05.personclasses;

public class Unemployed extends Person {

    public Unemployed(String name, String age) {
        super(name, Integer.parseInt(age));
    }
}
