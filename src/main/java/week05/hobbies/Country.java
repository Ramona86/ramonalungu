package week05.hobbies;

import java.util.ArrayList;
import java.util.List;

public class Country {
    String countries;
    String competitions;


    public Country(String countries, String competitions) {
        this.countries = countries;
        this.competitions = competitions;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countries='" + countries + '\'' +
                ", competitions='" + competitions + '\'' +
                '}';
    }
}
