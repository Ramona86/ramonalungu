package week05.hobbies;

import week05.hobbies.Address;
import week05.hobbies.Country;

import java.util.ArrayList;
import java.util.List;

public class Hobbies {
    private String cycling;
    private String singing;
    private String dancing;
    private int frequency;


    public List<Address> addressList = new ArrayList<>();
    public List<Country> countryList = new ArrayList<>();

    public String getCycling() {
        return cycling;
    }

    public void setCycling(String cycling) {
        this.cycling = cycling;
    }

    public String getSinging() {
        return singing;
    }

    public void setSinging(String singing) {
        this.singing = singing;
    }

    public String getDancing() {
        return dancing;
    }

    public void setDancing(String dancing) {
        this.dancing = dancing;
    }

    public void setAddressList(Address addressList) {
        this.addressList.add(addressList);
    }

    public void setCountryList(Country countryList) {
        this.countryList.add(countryList);
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Hobbies{" +
                "cycling='" + cycling + '\'' +
                ", singing='" + singing + '\'' +
                ", dancing='" + dancing + '\'' +
                ", frequency=" + frequency +
                ", addressList=" + addressList +
                ", countryList=" + countryList +
                '}';
    }
}
