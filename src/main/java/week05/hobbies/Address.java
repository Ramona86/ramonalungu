package week05.hobbies;

public class Address {
    private String street;
    private String number;
    private String town;


    public Address(String street, String number, String town) {
        this.street = street;
        this.number = number;
        this.town = town;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", town='" + town + '\'' +
                '}';
    }
}

