package week05.hobbies.comparators;

import week05.personclasses.Person;

import java.util.Comparator;

public class PersonNameComparator implements Comparator<Person> {

    public PersonNameComparator() {
    }

    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
