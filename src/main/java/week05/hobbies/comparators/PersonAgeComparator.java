package week05.hobbies.comparators;

import week05.personclasses.Person;

import java.util.Comparator;

public class PersonAgeComparator implements Comparator<Person> {

    public PersonAgeComparator() {
    }

    @Override
    public int compare(Person o1, Person o2) {
        if(o1.getAge() > o2.getAge()){
            return 1;
        } else {
            return -1;
        }
    }
}
