package main.java.ro.sci;

public abstract class Car extends Vehicle {
    protected String name;
    protected String chassisNumber;

    protected Car() {

    }

    public Car(String name, String chassisNumber) {
        this.name = name;
        this.chassisNumber = chassisNumber;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive() {

    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", chassisNumber=" + chassisNumber +
                '}';
    }
}
