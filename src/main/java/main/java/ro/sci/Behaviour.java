package main.java.ro.sci;

public interface Behaviour {

    void start();
    void drive(int distanceKm);
    void shiftGear(int currentGear);
    void stop();



}
