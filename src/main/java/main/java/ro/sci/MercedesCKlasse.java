package main.java.ro.sci;

public class MercedesCKlasse extends Mercedes{
    private static final String fuelType = "Diesel";

    public MercedesCKlasse(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    public String getFuelType() {
        return fuelType;
    }

    @Override
    public void start() {
        System.out.println("Start! The consumption stats are reset");
        System.out.println("The consum is:" + "" + 0);
    }

    public void drive(double v) {
        getAvailableFuel();
        System.out.println("Available fuel is" + " " + getAvailableFuel());


    }

    @Override
    public void shiftGear() {
    }


    @Override
    public void shiftGear(int currentGear) {
        getAvailableFuel();
        System.out.println("Available fuel is" + " " + getAvailableFuel());
    }

    @Override
    public void stop() {
        System.out.println("Car stopped");
    }
}
