package main.java.ro.sci;

public abstract class Volkswagen extends Car {

    private static final int fuelTankSize = 60;
    private static final int gears = 5;
    private static final float consumptionPer100Km = (float) 6.2;
    private static final int tireSize = 15;

    public Volkswagen(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive() {

    }
}
