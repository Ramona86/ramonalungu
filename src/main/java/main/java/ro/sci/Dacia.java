package main.java.ro.sci;

public abstract class Dacia extends Car implements Behaviour{

    private static final int fuelTankSize = 40;
    private static final int gears = 5;
    private static final float consumptionPer100Km = (float) 4.7;
    private static final int tireSize = 15;
    private int distanceKm;
    private int currentGear;
    private int availableFuel= 40;
    protected int averageFuelConsumption;

    public Dacia(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    public float getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public void setDistanceKm(int distanceKm) {
        this.distanceKm = distanceKm;
    }

    public void setCurrentGear(int currentGear) {
        this.currentGear = currentGear;
    }


  public float calculateConsumptionPer100Km(){
        setDistanceKm(100);
        setCurrentGear(1);
        if (tireSize == 15) {
            return getConsumptionPer100Km();
        }
      return getConsumptionPer100Km();
  }

    public double getAvailableFuel() {
        availableFuel = availableFuel -1;
        return availableFuel;
    }

    public int getAverageFuelConsumption() {
        averageFuelConsumption = (availableFuel * 100) / distanceKm;
        return averageFuelConsumption;
    }

    public int getFuelTankSize() {
        return fuelTankSize;
    }

    public int getGears() {
        return gears;
    }


    public int getTireSize() {
        return tireSize;
    }


    public int getCurrentGear() {
        return currentGear;
    }


    @Override
    public void start() {

    }

    @Override
    public void drive(int distanceKm) {


    }

    public abstract void shiftGear();

    @Override
    public void shiftGear(int currentGear) {

    }

    @Override
    public void stop() {

    }

}


