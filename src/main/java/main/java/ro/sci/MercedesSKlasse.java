package main.java.ro.sci;

public class MercedesSKlasse extends Mercedes{
    private static final String fuelType = "PETROL";

    public MercedesSKlasse(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    @Override
    public void start() {

    }

    @Override
    public void drive(int distanceKm) {

    }

    @Override
    public void shiftGear() {

    }

    @Override
    public void shiftGear(int currentGear) {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive() {

    }
}
