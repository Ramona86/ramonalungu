package main.java.ro.sci;

public class Main {
    public static void main(String[] args) {


        Duster car1 = new Duster("Duster", "6790000998");
        System.out.println(car1);

        car1.getFuelTankSize();
        System.out.println("Duster FuelTankSize is" + " " + car1.getFuelTankSize());

        car1.getFuelType();
        System.out.println("Duster FuelType is" + " " + car1.getFuelType());

        car1.calculateConsumptionPer100Km();
        System.out.println("Duster FuelType is" + " " + car1.calculateConsumptionPer100Km());

        car1.start();

        car1.shiftGear(1);
        car1.drive(0.01);// drives 0.01 KMs
        car1.shiftGear(2);
        car1.drive(0.02);
        car1.shiftGear(3);
        car1.drive(0.5);
        car1.shiftGear(4);
        car1.drive(0.5);
        car1.shiftGear(4);
        car1.drive(0.5);
        car1.shiftGear(5);
        car1.drive(10);
        car1.shiftGear(4);
        car1.drive(0.5);
        car1.shiftGear(3);
        car1.drive(0.1);

        car1.stop();


        double availableFuel = car1.getAvailableFuel();
        System.out.println(car1.getAvailableFuel());
        double fuelConsumedPer100Km = car1.getAverageFuelConsumption();
        System.out.println(car1.getAverageFuelConsumption());

        Logan car2 = new Logan("Logan", "288900876");
        System.out.println(car2);

        MercedesCKlasse car3 = new MercedesCKlasse("MercedesCKlasse", "7890000000");

        car3.getFuelTankSize();
        System.out.println("MercedesCKlasse FuelTankSize is" + " " + car3.getFuelTankSize());

        car3.getFuelType();
        System.out.println("MercedesCKlasse FuelType is" + " " + car3.getFuelType());

        car3.getConsumptionPer100Km();
        System.out.println("MercedesCKlasse ConsumptionPer100Km is" + " " + car3.getConsumptionPer100Km());

        car3.start();

        car3.shiftGear(1);
        car3.drive(0.01);// drives 0.01 KMs
        car3.shiftGear(2);
        car3.drive(0.02);
        car3.shiftGear(3);
        car3.drive(0.5);
        car3.shiftGear(4);
        car3.drive(0.5);
        car3.shiftGear(4);
        car3.drive(0.5);
        car3.shiftGear(5);
        car3.drive(10);
        car3.shiftGear(4);
        car3.drive(0.5);
        car3.shiftGear(3);
        car3.drive(0.1);

        car3.stop();


        Vehicle vehicle = new VWGolf("VWGolf", "678900990");

        vehicle.start();
        vehicle.drive(1);
        vehicle.stop();
        Car car = (Car) vehicle;
    }
}
