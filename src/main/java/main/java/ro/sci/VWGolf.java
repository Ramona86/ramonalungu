package main.java.ro.sci;

public class VWGolf extends Volkswagen {
    private final String fuelType = "Diesel";

    public VWGolf(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    @Override
    public void start() {

    }

    @Override
    public void drive(int distanceKm) {

    }

    @Override
    public void shiftGear(int currentGear) {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive() {

    }
}
