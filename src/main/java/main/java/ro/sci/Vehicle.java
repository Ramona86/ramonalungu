package main.java.ro.sci;

public abstract class Vehicle implements Behaviour {

    public abstract void start ();

    public abstract void stop ();

    public abstract void drive ();

}
