package main.java.ro.sci;

public class Logan extends Dacia{

    private static final String fuelType = "PETROL";

    public Logan(String name, String chassisNumber) {
        super(name, chassisNumber);
    }

    @Override
    public void start() {

    }


    @Override
    public void drive() {

    }

    @Override
    public void shiftGear() {

    }

    @Override
    public void stop() {

    }
}
