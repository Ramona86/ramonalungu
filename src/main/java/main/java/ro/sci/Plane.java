package main.java.ro.sci;

public class Plane extends Vehicle{
    @Override
    public void start() {

    }

    @Override
    public void drive(int distanceKm) {

    }

    @Override
    public void shiftGear(int currentGear) {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive() {

    }
}
