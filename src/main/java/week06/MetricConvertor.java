package week06;

public class MetricConvertor {
    protected int numberToConvert;
    protected String unitToConvert;
    protected int convertedNumber;
    protected int mm = 1;
    protected int cm;
    protected int dm;
    protected int m;
    protected int km;
    String plusOperator;
    String minusOperator;
    protected int numOne;
    protected int numTwo;
    protected int numThree;
    protected int firstExpression;
    protected int secondExpression;



    public int getNumberToConvert() {
        return numberToConvert;
    }

    public void setNumberToConvert(int numberToConvert) {
        this.numberToConvert = numberToConvert;
    }

    public String getUnitToConvert() {
        return unitToConvert;
    }

    public void setUnitToConvert(String unitToConvert) {
        this.unitToConvert = unitToConvert;
    }

    public int getConvertedNumber() {
        return convertedNumber;
    }

    public void setConvertedNumber(int convertedNumber) {
        this.convertedNumber = convertedNumber;
    }
    public String getPlusOperator() {
        return plusOperator;
    }

    public void setPlusOperator(String plusOperator) {
        this.plusOperator = plusOperator;
    }

    public String getMinusOperator() {
        return minusOperator;
    }

    public void setMinusOperator(String minusOperator) {
        this.minusOperator = minusOperator;
    }

    public int getNumOne() {
        return numOne;
    }

    public void setNumOne(int numOne) {
        this.numOne = numOne;
    }

    public int getNumTwo() {
        return numTwo;
    }

    public void setNumTwo(int numTwo) {
        this.numTwo = numTwo;
    }

    public int getNumThree() {
        return numThree;
    }

    public void setNumThree(int numThree) {
        this.numThree = numThree;
    }

    public int convertIntoMmFromKm() {
        if (unitToConvert.equalsIgnoreCase("km")) {
            this.numberToConvert = numberToConvert * (1000000 * mm);
        }
        return convertedNumber = this.numberToConvert;
    }

    public int convertIntoMmFromM() {
        if (unitToConvert.equalsIgnoreCase("m")) {
            this.numberToConvert = numberToConvert * (1000 * mm);
        }
        return convertedNumber = this.numberToConvert;
    }

    public int convertIntoMmFromDm() {
        if (unitToConvert.equalsIgnoreCase("dm")) {
            this.numberToConvert = numberToConvert * (100 * mm);
        }
        return convertedNumber = this.numberToConvert;

    }

    public int convertIntoMmFromCm() {
        if (unitToConvert.equalsIgnoreCase("cm")) {
            this.numberToConvert = numberToConvert * (10 * mm);
        }
        return convertedNumber = this.numberToConvert;
    }

    public int operationOne() {
        if (plusOperator.equals("+"))
             firstExpression = numOne + numTwo;
        else if (minusOperator.equals("-"))
            firstExpression=  numOne - numTwo;
        return firstExpression;
    }

    public int operationTwo() {
        if (minusOperator.equals("-")) {
            secondExpression = firstExpression - numThree;
            return secondExpression;
        } else {
            if (plusOperator.equals("+"))
                secondExpression = firstExpression + numThree;
        }
        return secondExpression;
    }

    @Override
    public String toString() {
        return "MetricConvertor{" +
                "numberToConvert=" + numberToConvert +
                ", unitToConvert='" + unitToConvert + '\'' +
                ", convertedNumber=" + convertedNumber +
                ", mm=" + mm +
                ", cm=" + cm +
                ", dm=" + dm +
                ", m=" + m +
                ", km=" + km +
                ", plusOperator='" + plusOperator + '\'' +
                ", minusOperator='" + minusOperator + '\'' +
                ", numOne=" + numOne +
                ", numTwo=" + numTwo +
                ", numThree=" + numThree +
                ", firstExpression=" + firstExpression +
                ", secondExpression=" + secondExpression +
                '}';
    }
}



