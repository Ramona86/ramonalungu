package week06;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the value and the unit you want to calculate with spaces between: ");
        String input = sc.nextLine();
        System.out.println(input);

//        split first input

        String[] splitStrings = input.split(" ");
//      Number:
        int x;
        x = Integer.parseInt(splitStrings[0]);
        System.out.println(x);

//        Unit:
        String unitA = splitStrings[1];
        System.out.println(unitA);

//        operator
        System.out.print("Enter the operator + or -: ");
        String operatorOne = sc.nextLine();
        System.out.println(operatorOne);

        System.out.print("Enter the value and the unit you want to calculate with spaces between: ");
        String secondInput = sc.nextLine();

//        split second input

        String[] splitStringsTwo = secondInput.split(" ");
//        Number:
        int y;
        y = Integer.parseInt(splitStringsTwo[0]);
        System.out.println(y);
//        Unit
        String unitB = splitStringsTwo[1];
        System.out.println(unitB);

//        operator

        System.out.print("Enter the operator + or -: ");
        String operatorTwo = sc.nextLine();
        System.out.println(operatorTwo);

        System.out.print("Enter the value and the unit you want to calculate with spaces between: ");
        String thirdInput = sc.nextLine();

//          split third input

        String[] splitStringsThree = thirdInput.split(" ");
//        Number
        int z;
        z = Integer.parseInt(splitStringsThree[0]);
        System.out.println(z);

//        Unit

        String unitC = splitStringsThree[1];
        System.out.println(unitC);

        MetricConvertor metricConvertor = new MetricConvertor();


//        System.out.println(inputNumber1+operatorOne+inputNumber2+operatorTwo+inputNumber3);

        metricConvertor.setNumberToConvert(x);
        metricConvertor.setUnitToConvert(unitA);
        System.out.println(metricConvertor.getNumberToConvert());
        System.out.println(metricConvertor.convertIntoMmFromKm());
        System.out.println(metricConvertor.convertIntoMmFromM());
        System.out.println(metricConvertor.convertIntoMmFromDm());
        System.out.println(metricConvertor.convertIntoMmFromCm());

        int numberConverted1 = metricConvertor.convertedNumber;
        System.out.println("Converted number1 is " + metricConvertor.convertedNumber);

        metricConvertor.setNumberToConvert(y);
        metricConvertor.setUnitToConvert(unitB);
        System.out.println(metricConvertor.getNumberToConvert());
        System.out.println(metricConvertor.convertIntoMmFromKm());
        System.out.println(metricConvertor.convertIntoMmFromM());
        System.out.println(metricConvertor.convertIntoMmFromDm());
        System.out.println(metricConvertor.convertIntoMmFromCm());

        int numberConverted2 = metricConvertor.convertedNumber;
        System.out.println("Converted number2 is " + metricConvertor.convertedNumber);

        metricConvertor.setNumberToConvert(z);
        metricConvertor.setUnitToConvert(unitC);
        System.out.println(metricConvertor.getNumberToConvert());
        System.out.println(metricConvertor.convertIntoMmFromKm());
        System.out.println(metricConvertor.convertIntoMmFromM());
        System.out.println(metricConvertor.convertIntoMmFromDm());
        System.out.println(metricConvertor.convertIntoMmFromCm());

        int numberConverted3 = metricConvertor.convertedNumber;
        System.out.println("Converted number3 is " + metricConvertor.convertedNumber);

        metricConvertor.setPlusOperator(operatorOne);
        metricConvertor.setMinusOperator(operatorTwo);
        System.out.println(operatorOne);
        System.out.println(operatorTwo);

        metricConvertor.setNumOne(numberConverted1);
        metricConvertor.setNumTwo(numberConverted2);
        metricConvertor.setNumThree(numberConverted3);

         metricConvertor.operationOne();
         metricConvertor.operationTwo();

        System.out.println(metricConvertor.firstExpression);
        System.out.println("Result is: " + metricConvertor.secondExpression);

    }
}