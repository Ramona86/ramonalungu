package week11;

public class FestivalAttendeeThread extends Thread{
    private TicketType ticket1;
    private FestivalGate gate1;

    public FestivalAttendeeThread(TicketType ticket1, FestivalGate gate1) {
        this.ticket1 = ticket1;
        this.gate1 = gate1;
    }

    public void run(TicketType ty) {
        System.out.println("The type of the ticket is: "+ ty.getTicketType());
    }
}
