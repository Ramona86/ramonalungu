package week11;

import java.lang.reflect.Type;

public class TicketType {
    public enum Type{
        FULL,
        FULL_VIP,
        FREE_PAS,
        ONE_DAY,
        ONE_DAY_VIP
    }
    private Type ticketType;

    public TicketType(Type ticketType) {
        this.ticketType = ticketType;
    }

    public Type getTicketType() {
        return ticketType;
    }

}
