package week11;

import javax.sound.sampled.LineEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException{

        FestivalGate gate = new FestivalGate();

        int [] index = {0,0,0,0,0};

        List<TicketType.Type> typeList= new ArrayList<>();
        typeList.add(TicketType.Type.FULL);
        typeList.add(TicketType.Type.FREE_PAS);
        typeList.add(TicketType.Type.FULL_VIP);
        typeList.add(TicketType.Type.ONE_DAY);
        typeList.add(TicketType.Type.ONE_DAY_VIP);

        Random generator = new Random();
        for (int i = 0; i <200 ; i++) {
            int myValue = generator.nextInt(0, 5);
            index[myValue]+=1;
            TicketType ticketType = new TicketType(typeList.get(myValue));
            FestivalAttendeeThread attendee = new FestivalAttendeeThread(ticketType, gate);
            attendee.run(ticketType);
            Thread.sleep(2000);

        }
        for (int i = 0; i < 5; i++) {
            System.out.println(typeList.get(i)+ ":" + index[i]);
        }
    }
}
