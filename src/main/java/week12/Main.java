package week12;

import java.sql.*;

public class Main {
    public static void main(String[] args) {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BookingApp", "postgres", "parola12345");
      } catch (SQLException e) {
            System.err.println("Can NOT connect to DB" + e.getMessage());
        }

        if (connection == null) {
            return;
        } 

        Statement statement = null;
        ResultSet resultSet = null;
        final String format = "%50s%50s%12s\n";

        try {
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery("select * from accommodation");

            boolean hasResults = resultSet.next();
            if (hasResults) {
                System.out.format(format, "id", "type", "bed_type", "max_guests","description" );
                do {
                    System.out.format(format,
                            resultSet.getString("id"),
                            resultSet.getString("type"),
                            resultSet.getString("bed_type"),
                            resultSet.getString("max_guests"),
                            resultSet.getString("description"));
                } while (resultSet.next());
            } else {
                System.out.println("No results");
            }

            resultSet = statement.executeQuery("select * from room_fair");
            hasResults = resultSet.next();

            if (hasResults) {
                System.out.format(format, "id", "value", "season");
                do {
                    System.out.format(format,
                            resultSet.getString("id"),
                            resultSet.getString("value"),
                            resultSet.getString("season"));
                } while (resultSet.next());
            } else {
                System.out.println("No results");
            }

            resultSet = statement.executeQuery("select * from accommodation_room_fair_relation");
            hasResults = resultSet.next();

            if (hasResults) {
                System.out.format(format, "id", "accommodation_id", "room_fair_id" );
                do {
                    System.out.format(format,
                            resultSet.getString("id"),
                            resultSet.getString("accommodation_id"),
                            resultSet.getString("room_fair_id"));
                } while (resultSet.next());
            } else {
                System.out.println("No results");
            }

            resultSet = statement.executeQuery("select accommodation.id, accommodation.type FROM accommodation\n" +
                    "Join room_fair\n" +
                    "on accommodation.id = room_fair.id\n");
//            hasResults = resultSet.next();

            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("insert into accommodation values (?,?,?, ?, ?)");
                preparedStatement.setInt(1, 5);
                preparedStatement.setString(2, "Superior King Room");
                preparedStatement.setString(3, "1 large double bed");
                preparedStatement.setInt(4, 2);
                preparedStatement.setString(5, "This double room has a flat-screen TV, bathrobe and soundproofing.");
                preparedStatement.executeUpdate();
            } catch (
    SQLException e) {
                System.err.println("Can not insert accommodation " + e.getMessage());
            }

        } catch (SQLException e) {
            System.err.println("Can not run query" + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }
}
