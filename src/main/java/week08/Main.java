package week08;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;



public class Main {
    public static void main(String[] args) {
        List<Athlete> athletesList = readAthletesFromCSV("CSVfile.csv");

        for (Athlete a : athletesList) {
            System.out.println(a);
        }
    }

    private static List<Athlete> readAthletesFromCSV(String CSVfile) {

        List<Athlete> athletes = new ArrayList<>();
        Path pathToFile = Paths.get(CSVfile);

        // create an instance of BufferedReader
        // using try with resource, Java 7 feature to close resources
        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Ramo\\Desktop\\CSVfile.csv"));

            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
            while (line != null) {                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
                String[] attributes = line.split(",");
                System.out.println("Athletes [AthleteNumber=" + attributes[0] + ", AthleteName=" + attributes[1] + ", CountryCode=" + attributes[2] + ", SkiTimeResult=" + attributes[3] + ", FirstShootingRange= " + attributes[4] + ", SecondShooting= " + attributes[5] + ", ThirdShooting= " + attributes[6] + "]");


                Athlete athlete = createAthlete(attributes);

                // adding book into ArrayList
                athletes.add(athlete);

                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();
            }


        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return athletes;
    }

    private static Athlete createAthlete(String[] attribute) {
        String athleteNumber = attribute[0];
        String athleteName = attribute[1];
        String countryCode = attribute[2];
        String skiTimeResult = attribute[3];
        String firstShootingRange = attribute[4];
        String secondShooting = attribute[5];
        String thirdShooting = attribute[6];

        System.out.println(athleteName);
        System.out.println(skiTimeResult);
        System.out.println(firstShootingRange);

        // create and return athlete of this metadata
        return new Athlete(athleteName, athleteNumber, countryCode, skiTimeResult, firstShootingRange,secondShooting, thirdShooting);
    }

}


