package week08;

public enum Scores {

        HIT("X"),
        MISS("0");

        private String results;


        Scores(String results) {
            this.results = results;
        }


        @Override
        public String toString() {
                return "Scores{" +
                        "results='" + results + '\'' +
                        '}';
        }
}
