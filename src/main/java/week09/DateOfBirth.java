package week09;

public class DateOfBirth {
    private int yBirth;
    private int mBirth;
    private int dBirth;

    public DateOfBirth(int yBirth, int mBirth, int dBirth) {
        this.yBirth = yBirth;
        this.mBirth = mBirth;
        this.dBirth = dBirth;
    }

    public int getyBirth() {
        return yBirth;
    }

    public void setyBirth(int yBirth) {
        this.yBirth = yBirth;
    }

    public int getmBirth() {
        return mBirth;
    }

    public void setmBirth(int mBirth) {
        this.mBirth = mBirth;
    }

    public int getdBirth() {
        return dBirth;
    }

    public void setdBirth(int dBirth) {
        this.dBirth = dBirth;
    }

    @Override
    public String toString() {
        return "DateOfBirth{" +
                "yBirth=" + yBirth +
                ", mBirth=" + mBirth +
                ", dBirth=" + dBirth +
                '}';
    }
}
