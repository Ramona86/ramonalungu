package week09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

//    final static Logger logger = (Logger) LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
//            logger.info("These are my logs");

        List<Student> students = new ArrayList<>();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("Repo.csv"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(",");

                Student student= new Student(split[0], split[1], split[2], split[3], split[4]);
                students.add(student);

                List<String> dOB= new ArrayList<>();
                String[] split2 = split[3].split("-");
                dOB.add(split[3]);
                System.out.println(dOB);

                DateOfBirth dateOfBirth = new DateOfBirth(Integer.parseInt(split2[2]), Integer.parseInt(split2[01]), Integer.parseInt(split2[0]));
                System.out.println(dateOfBirth);


                LocalDate today = LocalDate.now();
                LocalDate birthdate = LocalDate.of(dateOfBirth.getyBirth(), dateOfBirth.getmBirth(), dateOfBirth.getdBirth());
                long years = ChronoUnit.YEARS.between(birthdate, today);

                System.out.println("Age in years is:" + years);

                Period period = Period.between(birthdate, today);
                System.out.println("Year:"+ period.getYears() + "Month:" +period.getMonths()+ "Days:" + period.getDays());

                Student student1= new Student(Math.toIntExact(years));
                Student student2 = new Student(split[0], split[1], split[2], split[3]); //              students with ID deleted

                List < Student > sortedDescending = students.stream()
                        .sorted(Comparator.comparing(Student::getFirstName).thenComparing(Student::getFirstName).reversed())
                        .collect(Collectors.toList());
                sortedDescending.forEach(System.out::println); //students sorted by firstname
            }
        } catch (
                IOException exception) {
            System.err.println("Error" + exception.getMessage());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


        }
    }
}
