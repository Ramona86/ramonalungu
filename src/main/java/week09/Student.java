package week09;

import java.util.EmptyStackException;

public class Student {

    private String firstName;
    private String lastName;
    private String gender;
    private String dateOfBirth;
    private String CNP;
    private int years;

    public Student(int years) {
        this.years = years;
        if(years<18)
            try{
                throw new Exception();
            }catch(Exception e){
                System.out.println("date of birth between 1900 and current year - 18 ");
            }
        else
            System.out.println("age is correct");
    }


    public Student(String firstName, String lastName, String gender, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.years = years;

        this.CNP=CNP;

            try{
                if(CNP == null || CNP.isEmpty());
            } catch (EmptyStackException | NullPointerException  exc) {
            System.out.println("Empty");
        }

    }

    public Student(String firstName, String lastName, String gender, String dateOfBirth, String CNP) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender =  gender;
        this.dateOfBirth = dateOfBirth;
        this.CNP = CNP;
    }

    public Student() {

    }


    public String getFirstName() {
        return firstName;
    }

    public int getYears() {
        return years;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", CNP='" + CNP + '\'' +
                ", years=" + years +
                '}';
    }

}

