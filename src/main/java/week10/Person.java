package week10;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Person {
    private String firstName;
    private String lastName;
    private String birthdate;



    public Person(String firstName, String lastName, String birthdate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;}

    void saveContact(String firstName, String lastName, String birthdate, String file){
        String row = firstName + "," + lastName + "," + birthdate +"\n";
        writeRow(row);
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }


    void writeRow(String row) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("Repo2.csv", true));
            {
                out.write(row);
                System.out.println(row);
                List<String> persons = new ArrayList<>();
                persons.add(row);
                System.out.println(persons);
                PrintWriter printWriter = new PrintWriter(out);
                printWriter.flush();
                printWriter.close();

            }
        } catch (IOException e) {
            throw new RuntimeException(e);


        }


    }

}
