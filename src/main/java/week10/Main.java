package week10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class Main {
    public static void main(String[] args) {

        List<Person> persons= new ArrayList<>();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("CSVfile3.csv"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(",");

                Person person = new Person(split[0], split[1], split[2]);
                persons.add(person);
                System.out.println(persons);

                List<String> dOB= new ArrayList<>();
                dOB.add(split[2]);
                String[] split2 = split[2].split("-");
                System.out.println("List DOB:" + dOB);

                Birthdate birthdate = new Birthdate((split2[0]), (split2[01]), (split2[2]));
                System.out.println((birthdate.getMonth()));

                List <String> monthList=new ArrayList<>();
                monthList.add(birthdate.getMonth());
                System.out.println(monthList);

//                int monthToBeSorted =12;
//                List<int>filteredMonths = monthList.stream();
//                        .filter(p-> {"12".equals(birthdate.getMonth())
//                        .collect.toList();


            }
        } catch (
                IOException exception) {
            System.err.println("Error" + exception.getMessage());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


        }
    }


}

