package quizz2;

public class Package {
    private String targetLocation;
    private String targetDistance;
    private String packageValue;
    private String deliveryDate;


    public Package(String targetLocation, String targetDistance, String packageValue, String deliveryDate) {
        this.targetLocation = targetLocation;
        this.targetDistance = targetDistance;
        this.packageValue = packageValue;
        this.deliveryDate = deliveryDate;
    }
}
